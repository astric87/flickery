//
//  FlickeryUITests.swift
//  FlickeryUITests
//
//  Created by Aaron Strickland on 21/11/2023.
//

import XCTest


class FlickeryUITests: XCTestCase {

    override func setUpWithError() throws {
        continueAfterFailure = false
        XCUIApplication().launch()
    }

    func testGalleryView() throws {
        let app = XCUIApplication()
        
        

        // Assuming there's a button with gear icon for settings
        let settingsButton = app.buttons["gear"]
        
        
        XCTAssertTrue(settingsButton.exists)

        settingsButton.tap()

        // Assuming there's a close button with multiply.circle.fill icon in the settings view
        let closeButton = app.buttons["multiply.circle.fill"]
        XCTAssertTrue(closeButton.exists)

        closeButton.tap()

        // Assuming there's a search bar
        let searchBar = app.searchFields["Search..."]
        XCTAssertTrue(searchBar.exists)

        // Perform a search in the search bar
        searchBar.tap()
        searchBar.typeText("London")
        app.buttons["Search"].tap()

        // Assuming there's an image preview in the gallery
        let imagePreview = app.images["ImagePreview"]
        XCTAssertTrue(imagePreview.exists)

        // Tap on the image preview to navigate to the detail view
        imagePreview.tap()

        // Assuming there's a back button to return to the gallery view
        let backButton = app.buttons["Back"]
        XCTAssertTrue(backButton.exists)
        backButton.tap()
    }
}
