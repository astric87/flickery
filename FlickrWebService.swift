// WebService.swift
// Flickery
//
// Created by Aaron Strickland on 21/11/2023.

import Foundation
import SwiftUI


protocol Networking {
    func fetchData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> Void)
}

let apiKey = AppConfig.shared.flickrAPIKey()

class FlickrWebService: Networking {
    // FLICKR API KEY in INFO.plist
    
    // MARK: - Properties
    // Base URL for the Flickr API for our usecase
    var baseURL: String = "\(AppConfig.shared.baseURL)feeds/photos_public.gne?format=json&&api_key=\(apiKey)&nojsoncallback=1&"
    @AppStorage("searchBy") internal var searchBy = "tags"
    
    @AppStorage("safeSearchSetting") internal var safeSearchSetting = 0
    
    // MARK: - Networking Methods
    
    /// Fetch data from a given URL using URLSession. Conforming to the `Networking` protocol.
    internal func fetchData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        print("datadog \(url)")
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                completion(nil, response, FlickrError.networkError(description: error.localizedDescription))
                return
            }
            completion(data, response, nil)
        }.resume()
    }

    /// Handle image data by decoding it into an array of PhotoItem objects.
    internal func handleImageData(_ data: Data?, completion: @escaping ([PhotoItem]) -> Void) {
        guard let data = data else {
            Logger.debug("Error: Data not found")
            return
        }
        
        do {
            let imageWrapper = try JSONDecoder().decode(PhotoWrapper.self, from: data)
            DispatchQueue.main.async {
                completion(imageWrapper.items)
            }
        } catch {
            Logger.debug("Error parsing data: \(error)")
        }
    }
    
    // MARK: - Public Methods
    
    /// Fetch images based on search text and optional user ID.
    func getImages(with searchText: String, userID: String?, completion: @escaping ([PhotoItem]) -> Void, failure: @escaping (Error) -> Void) {
        // Construct the URL based on the search text and optional user ID
        var urlString = "\(baseURL)\(searchBy)=\(searchText)&safe_search=\(safeSearchSetting)"
        
        // If userID is not nil, the API request is for user images
        if let userID = userID {
            urlString = "\(baseURL)&id=\(userID)"
        } else {
            Logger.debug("UserID is not provided")
        }
        
        // Convert the constructed URL string to a URL object
        guard let url = URL(string: urlString) else {
            // If URL construction fails, create an error and call the failure closure
            let urlError = FlickrError.urlConstructionError
            failure(urlError)
            return
        }
        
        // Fetch data from the constructed URL using URLSession
        fetchData(from: url) { data, response, error in
            if let error = error {
                // If there is an error during data fetching, call the failure closure with a more descriptive error
                failure(FlickrError.networkError(description: error.localizedDescription))
            } else {
                // If data fetching is successful, handle the image data
                self.handleImageData(data, completion: completion)
            }
        }
    }
    
    /// Fetch user information.
    func getProfile(userID: String, completion: @escaping (UserInfo?, Error?) -> Void) {
      
        let baseURL = AppConfig.shared.baseURL
        let method = "flickr.people.getInfo"
        
        
        let formattedURL = "\(baseURL)rest/?method=\(method)&api_key=\(apiKey)&user_id=\(userID)&format=json&nojsoncallback=1"
    

        guard let url = URL(string: formattedURL) else {
            // If URL construction fails, create an error and call the completion closure with a more descriptive error
            completion(nil, FlickrError.urlConstructionError)
            return
        }

        // Fetch data from the constructed URL using URLSession
        fetchData(from: url) { data, _, error in
            if let error = error {
                // If there is an error during data fetching, call the completion closure with a more descriptive error
                completion(nil, FlickrError.networkError(description: error.localizedDescription))
                return
            }

            do {
                let decoder = JSONDecoder()
                let response = try decoder.decode(UserInfo.self, from: data!)

                completion(response, nil)
            } catch {
                // If there is an error during decoding, call the completion closure with a more descriptive error
                completion(nil, FlickrError.decodingError(description: error.localizedDescription))
            }
        }
    }
}


enum FlickrError: Error {
    case urlConstructionError
    case networkError(description: String)
    case decodingError(description: String)
    var localizedDescription: String {
      
        switch self {
        case .urlConstructionError:
            return "Failed to construct URL."
        case .networkError(let description):
            return "Network error: \(description)"
        case .decodingError(let description):
            return "Decoding error: \(description)"
        }
    }
}
