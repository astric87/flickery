// TagList.swift
// Flickery
//
// Created by Aaron Strickland on 21/11/2023.

import SwiftUI

/// A subview containing a list of tags. Tags flow onto the next line when wider than the view's width.
struct TagList: View {
    
    @Binding var tags: [String]
    
    // MARK: - Body
    
    var body: some View {
        GeometryReader { geo in
            generateTags(in: geo)
        }
    }

    /// Adds a tag view for each tag in the array. Tags populate from left to right and then onto new rows when too wide for the screen.
    ///
    /// - Parameter geo: The geometry proxy used to calculate layout.
    /// - Returns: A view containing tags arranged based on available space.
    private func generateTags(in geo: GeometryProxy) -> some View {
        var currentWidth: CGFloat = 0
        var currentHeight: CGFloat = 0

        return ZStack(alignment: .topLeading) {
            ForEach(tags, id: \.self) { tag in
                TagView(tag: tag, tags: $tags)
                    .alignmentGuide(.leading, computeValue: { tagSize in
                        if (abs(currentWidth - tagSize.width) > geo.size.width) {
                            currentWidth = 0
                            currentHeight -= tagSize.height
                        }
                        let offset = currentWidth
                        if tag == tags.last ?? "" {
                            currentWidth = 0
                        } else {
                            currentWidth -= tagSize.width
                        }
                        return offset
                    })
                    .alignmentGuide(.top, computeValue: { tagSize in
                        let offset = currentHeight
                        if tag == tags.last ?? "" {
                            currentHeight = 0
                        }
                        return offset
                    })
            }
        }
    }
}

/// A subview representing a tag in a list. When tapped, the tag will be removed from the array.
struct TagView: View {
    
    // MARK: - Properties
    
    var tag: String
    @Binding var tags: [String]
    
    // MARK: - Body
    
    var body: some View {
        Text(tag.lowercased())
            .font(.callout)
            .foregroundColor(.white)
            .padding(9)
            .background(
                Capsule()
                    .foregroundColor(.burnedOrange)
                    .clipped()
            )
            .clipShape(Capsule())
            .padding(5)
    }
}
