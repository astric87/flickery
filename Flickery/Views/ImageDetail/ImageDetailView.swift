// DetailView.swift
// Flickery
//
// Created by Aaron Strickland on 21/11/2023.
//

import SwiftUI

struct ImageDetailView: View {
    // MARK: - Properties
    
    @ObservedObject var detailVM = DetailViewModel()
    let imageItem: PhotoItem
    @State private var tags: [String] = []
    @State var selectedTag: String = ""
    @State var showUserPhotos = false

    // MARK: - Body
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            
            // Display the image with AsyncImage
            AsyncImage(url: URL(string: imageItem.media.urlString ?? "https://i.pravatar.cc/300")) { image in
                image
                    .resizable()
                    .frame(width: UIScreen.main.bounds.width, height: 300)
            } placeholder: {
                ProgressView()
            }
            
            VStack(alignment: .leading) {
                // Image title
                Text(imageItem.title.trimTitleLength())
                    .font(.title3)
                    .fontWeight(.bold)
                
                // Author information and "View photos" button
                HStack {
                    if let userInfo = detailVM.userInfo {
                        AvatarView(userInfo: userInfo.person, size: 40)
                    }
                    Text(imageItem.author.extractUsername() ?? imageItem.author)
                    
                    Spacer()
                    
                    Button {
                        self.showUserPhotos.toggle()
                    } label: {
                        Text("View photos")
                            .font(.caption)
                            .foregroundColor(.white)
                            .padding(9)
                            .background(
                                Capsule()
                                    .foregroundColor(.burnedOrange)
                                    .clipped()
                            )
                            .clipShape(Capsule())
                            .padding(5)
                    }
                }
                
                // Published date
                HStack {
                    Image(systemName: "clock")
                        .frame(width: 20, height: 20)
                    Text(imageItem.published.formatDateString() ?? imageItem.published)
                        .fontWeight(.medium)
                }
                
                // Tags
                if !imageItem.tags.isEmpty {
                    VStack(alignment: .leading) {
                        HStack {
                            Image(systemName: "tag")
                                .frame(width: 20, height: 20)
                            Text("Tags")
                        }
                        TagList(tags: $tags)
                            .onAppear() {
                                tags = wordsFromString(imageItem.tags)
                            }
                    }
                }
            }.padding(.horizontal, 10)
        }
        .edgesIgnoringSafeArea(.top)
        
        // Display user photos in a sheet
        .sheet(isPresented: $showUserPhotos) {
            SheetView(isPresented: $showUserPhotos, content: {
                VStack{
                    ScrollView {
                        ForEach(detailVM.images) { image in
                            GeometryReader { geometry in
                                AsyncImage(url: URL(string: image.media.urlString ?? "https://i.pravatar.cc/300")) { image in
                                    image
                                        .resizable()
                                        .scaledToFill()
                                        .frame(width: geometry.size.width, height: 250, alignment: .center)
                                        .clipped()
                                } placeholder: {
                                    ProgressView()
                                }
                            }
                            .frame(height: 250)
                        }
                    }
                }
            }, title: imageItem.author.extractUsername() ?? imageItem.author)
        }
        .onAppear(){
            // Fetch user information and images
            detailVM.getUserInfo(imageItem.authorId)
            detailVM.getUserImages(imageItem.authorId)
        }
    }

    // Helper method to extract words from a string
    func wordsFromString(_ inputString: String) -> [String] {
        return inputString.components(separatedBy: .whitespaces)
    }
}
