//
//  GalleryView.swift
//  Flickery
//
//  Created by Aaron Strickland on 21/11/2023.
//

import SwiftUI

// MARK: - GalleryView

struct GalleryView: View {
    
    // MARK: - Properties
    @EnvironmentObject var galleryVM: GalleryViewModel
    @Environment(\.scenePhase) var scenePhase

    
    @State private var searchText = ""
    @State private var isSearching = false
    @State private var showingSettingsSheet = false
    
    @AppStorage("firstLaunch") var firstLaunch = false
    @State private var placeholder = "Search..."
    
    
    
    
    private let threeColumnGrid = [
        GridItem(.flexible(minimum: 40)),
        GridItem(.flexible(minimum: 40)),
        GridItem(.flexible(minimum: 40)),
    ]
    // MARK: - Body
    
    var body: some View {
        
        // MARK: Image Grid
        NavigationStack {
            
            
            
            
            ScrollView(showsIndicators: false) {
                LazyVGrid(columns: threeColumnGrid, alignment: .center) {
                    ForEach(galleryVM.images, id: \.id) { photo in
                        GeometryReader { gr in
                            NavigationLink(destination: ImageDetailView(imageItem: photo)) {
                                // Each photo in the grid links to ImageDetailView
                                PhotoPreview(photo: photo)
                                
                            }
                        }
                        .clipped()
                        .aspectRatio(1, contentMode: .fit)
                    }
                }
            }
            .padding()
            .navigationTitle("Gallery")
            .toolbar {
                // Toolbar button for showing settings
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button(action: {
                        self.showingSettingsSheet.toggle()
                    }) {
                        Image(systemName: "gear")
                            .renderingMode(.original)
                            .frame(width: 30, height: 30)
                    }
                }
            }
        }
        .searchable(text: $searchText, prompt: placeholder)
        .onSubmit(of: .search) {
            // Perform search when the user submits the search
            galleryVM.getImages(searchText)
        }
        .sheet(isPresented: $showingSettingsSheet) {
            SheetView(isPresented: $showingSettingsSheet, content: {
                       // Your custom view goes here
                 SettingsView()
            }, title: "Settings")
          }

        .refreshable {
            // Refresh button to show random count
            galleryVM.getImages()
        }
        
        .onChange(of: searchText) {
            // Perform search when the search text changes
            galleryVM.getImages(searchText)
        }
        
        .onChange(of: scenePhase) { oldValue, newValue in
            if newValue == .active {
                galleryVM.getImages()
            }
        }
        
        
    
    }
}

    
// MARK: - PhotoPreview
struct PhotoPreview: View {
   
    let photo: PhotoItem

    var body: some View {
        // Display each photo in a resizable container
        ZStack(alignment: .topTrailing) {
            AsyncImage(url: URL(string: photo.media.urlString ?? "https://i.pravatar.cc/300")) { response in
                if let image = response.image {
                    image
                        .resizable()
                        .scaledToFill()
                       
                }
                else if response.error != nil {
                    
                    // do something more here....
                    VStack(alignment: .center){
                        Spacer()
                        Image(systemName: "exclamationmark.triangle.fill")
                            .resizable()
                            .frame(width: 50, height: 50)
                        Text("Loading failed")
                        Spacer()
                    }.foregroundColor(.red)
                     
                   
                }else{
                    ProgressView()
                }
                
                
               
            }
           
        }
    }
}


// MARK: - Preview
#Preview {
    GalleryView()
}




