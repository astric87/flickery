// AvatarView.swift
// Flickery
//
// Created by Aaron Strickland on 23/11/2023.

import SwiftUI

struct AvatarView: View {
    
    // MARK: - Properties
    
    let userInfo: UserInfo.FlickrPerson
    
    let size: CGFloat
    
    // MARK: - Body
    
    var body: some View {
        // Display user profile picture if available, otherwise show a default placeholder
        if let profileURL = profilePictureURL(for: userInfo) {
            AsyncImage(url: profileURL) { image in
                image
                    .resizable()
                    .scaledToFit()
                    .frame(width: size, height: size)
                    .clipShape(Circle())
                    .padding()
            } placeholder: {
                ProgressView()
            }
            .frame(width: size, height: size)
        } else {
            // Display a default placeholder if no custom icon is available
            Image(systemName: "person.circle.fill")
                .resizable()
                .scaledToFit()
                .frame(width: size, height: size)
                .clipShape(Circle())
                .padding()
                .foregroundColor(.gray)
        }
        
    }
    
    // MARK: - Private Methods
    
    /// Get the URL for the user's profile picture based on Flickr information.
    ///
    /// - Parameter userInfo: Flickr user information.
    /// - Returns: The URL for the user's profile picture.
    private func profilePictureURL(for userInfo: UserInfo.FlickrPerson) -> URL? {
        // Check if custom icon is available, otherwise use the default icon
        if userInfo.iconserver != "0" {
            return URL(string: "https://farm\(userInfo.iconfarm).staticflickr.com/\(userInfo.iconserver)/buddyicons/\(userInfo.nsid).jpg")
        } else {
            Logger.debug("Failed to retrieve profile picture")
            return nil
        }
    }
}
