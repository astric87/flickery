//
//  SheetView.swift
//  Flickery
//
//  Created by Aaron Strickland on 26/11/2023.
//

import SwiftUI


struct SheetView<Content: View>: View {
    @Binding var isPresented: Bool
    @ViewBuilder let content: () -> Content
    var title: String
    var body: some View {
        NavigationStack {
            content()
                .toolbar {
                    ToolbarItem(placement: .topBarTrailing) {
                        Button {
                            self.isPresented.toggle()
                        } label: {
                            Image(systemName: "multiply.circle.fill")
                                .resizable()
                                .frame(width: 25, height: 25)
                                .foregroundColor(.burnedOrange)
                        }
                    }
                }
                .navigationTitle(title)
        }
        .interactiveDismissDisabled()
    }
}
