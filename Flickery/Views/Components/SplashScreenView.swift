// SplashScreenView.swift
// Flickery
//
// Created by Aaron Strickland on 23/11/2023.

import SwiftUI

struct SplashScreenView: View {
    // MARK: - Properties
    @Binding var isSplashScreenPresented: Bool
    @State private var size = 0.4
    @State private var opacity = 0.5
    
 

    // MARK: - Body
    var body: some View {
        VStack(spacing: -10) {
            HStack {
                Image("FlickeryIcon")
                    .renderingMode(.original)
                    .resizable()
                    .frame(width: 150, height: 150)
                    .cornerRadius(30)
            }

            HStack {
                Text("Flickry")
                    .font(.system(size: 30))
                    .minimumScaleFactor(0.7)
            }
            .padding()
        }
        .scaleEffect(size)
        .opacity(opacity)
        .onAppear {
            // Initial animation for scaling and opacity
            withAnimation(.linear(duration: 0.3)) {
                self.size = 0.9
                self.opacity = 1.0
            }
        }
        .onAppear {
            // Delayed animation to hide the splash screen
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                withAnimation {
                    self.isSplashScreenPresented = false
                }
            }
            
            
            // initial load of images
        }
    }
}
