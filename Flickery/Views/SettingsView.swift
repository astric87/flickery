//
//  SettingsView.swift
//  Flickery
//
//  Created by Aaron Strickland on 21/11/2023.
//

import SwiftUI

struct SettingsView: View {
    
    
    @AppStorage("safeSearchSetting") internal var safeSearchSetting = 1
    @AppStorage("searchBy") internal var searchBy = "tags"
    
    let safeSearchOptions: [(String, Int)] = [
          ("Safe", 1),
          ("Moderate", 2),
          ("Restricted", 3)
      ]
    
    let searchOptions: [(String, String)] = [
          ("Tag", "tags"),
          ("Text","text")
   
      ]

    var body: some View {
        NavigationStack {
            List {
                
                Section(header: Text("Search By")){
                    Picker("Search By", selection: $searchBy) {
                        ForEach(searchOptions, id: \.1) { option in
                            Text(option.0)
                                .tag(option.1)
                            
                        }
                    }.pickerStyle(.segmented)
                }
            
                Picker("Safe Search Settings", selection: $safeSearchSetting) {
                    ForEach(safeSearchOptions, id: \.1) { option in
                        Text(option.0)
                            .tag(option.1)
                    }
                }
                .pickerStyle(.inline)

                
             
            }
                .navigationTitle("Settings")
        }
    }
}

#Preview {
    GalleryView()
}
