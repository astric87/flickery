//
//  ProfileViewModel.swift
//  Flickery
//
//  Created by Aaron Strickland on 21/11/2023.
//

import Foundation
import UIKit

class DetailViewModel: ObservableObject {
    @Published var images = [PhotoItem]()
    @Published var userInfo: UserInfo?
    @Published var errorMessage: String?

    private let flickrWebService = FlickrWebService()

    func getUserImages(_ userID: String) {
        Logger.debug("getImages uuid \(userID)")
        flickrWebService.getImages(with: userID, userID: userID) { [weak self] images in
            DispatchQueue.main.async {
                self?.images = images
                self?.errorMessage = nil // Clear any previous error message
            }
        } failure: { [weak self] error in
            // Handle error case
            DispatchQueue.main.async {
                self?.handleError(error)
            }
        }
    }

    func getUserInfo(_ userID: String) {

        flickrWebService.getProfile(userID: userID) { [weak self] userInfo, error in
            if let userInfo = userInfo {
                DispatchQueue.main.async {
                    self?.userInfo = userInfo
                    self?.errorMessage = nil // Clear any previous error message
                }
            } else if let error = error {
                // Handle specific error types and provide user-friendly messages
                DispatchQueue.main.async {
                    self?.handleError(error)
                }
            }
        }
    }

    private func handleError(_ error: Error) {
        self.errorMessage = ViewModelHelper.errorMessage(for: error)
        Logger.debug("Error: \(self.errorMessage)")
        
    }
}





