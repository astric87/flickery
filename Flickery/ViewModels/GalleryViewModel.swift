//
//  ImageViewModel.swift
//  Flickery
//
//  Created by Aaron Strickland on 21/11/2023.
//

import Foundation

class GalleryViewModel: ObservableObject {
    @Published var images = [PhotoItem]()
    @Published var errorMessage: String?

    init() {
        getImages()
    }

    func getImages(_ text: String = "Yorkshire") {
        let flickrWebService = FlickrWebService()

        flickrWebService.getImages(with: text, userID: nil) { [weak self] images in
            // Successful response
            DispatchQueue.main.async {
                self?.images = images
                self?.errorMessage = nil // Clear any previous error message
                print("Got \(images.count) images ")
            }
        } failure: { [weak self] error in
            // Handle error case
            DispatchQueue.main.async {
                self?.errorMessage = ViewModelHelper.errorMessage(for: error)
                print("Error fetching images: \(self?.errorMessage ?? "Unknown Error")")
            }
        }
    }
}
