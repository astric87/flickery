//
//  String+Extension.swift
//  Flickery
//
//  Created by Aaron Strickland on 21/11/2023.
//

import Foundation


extension String {
    func formatDateString(inputFormat: String = "yyyy-MM-dd'T'HH:mm:ssZ", outputFormat: String = "d MMM, yyyy") -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = inputFormat

        if let date = dateFormatter.date(from: self) {
            dateFormatter.dateFormat = outputFormat
            return dateFormatter.string(from: date)
        }

        return nil
    }
    
    func extractUsername() -> String? {
           let pattern = "\"(.*?)\""
           
           do {
               let regex = try NSRegularExpression(pattern: pattern)
               if let match = regex.firstMatch(in: self, range: NSRange(self.startIndex..., in: self)) {
                   let range = Range(match.range(at: 1), in: self)
                   return range.map { String(self[$0]) }
               }
           } catch {
               Logger.debug("Error creating regular expression: \(error.localizedDescription)")
           }
           
           return nil
       }
    
    
    
    func trimTitleLength() -> String {
            if count > 25 {
                let index = self.index(startIndex, offsetBy: 15)
                return String(prefix(upTo: index))
            } else {
                return self
            }
        }
    
    
    func convertEpochToDaysSince() -> String? {
           // Convert epoch string to TimeInterval (Double)
           guard let epochTimeInterval = TimeInterval(self) else {
               return "Invalid epoch timestamp"
           }

           // Convert TimeInterval to Date
           let date = Date(timeIntervalSince1970: epochTimeInterval)

           // Calculate the number of days since the date
           let currentDate = Date()
           let calendar = Calendar.current
           let components = calendar.dateComponents([.day], from: date, to: currentDate)

           guard let daysSince = components.day else {
               return "Error calculating days since"
           }

           return "\(daysSince)"
       }
}
