//
//  UINavigationController+Extension.swift
//  Flickery
//
//  Created by Aaron Strickland on 21/11/2023.
//

import UIKit

extension UINavigationController {
    
    
    open override func viewWillLayoutSubviews() {
        navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}


