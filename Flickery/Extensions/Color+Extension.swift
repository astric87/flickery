//
//  Color+Extension.swift
//  Flickery
//
//  Created by Aaron Strickland on 23/11/2023.
//

import SwiftUI

extension Color {
    
    static var burnedOrange = Color("burntOrange")

}
