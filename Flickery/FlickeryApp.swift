//
//  FlickeryApp.swift
//  Flickery
//
//  Created by Aaron Strickland on 21/11/2023.
//

import SwiftUI

@main
struct FlickeryApp: App {
    
    
    @StateObject var galleryVM = GalleryViewModel()
    
    @State private var isSplashScreenPresented = true
    
     let screenWidth = UIScreen.main.bounds.size.width
     let screenHeight = UIScreen.main.bounds.size.height
    
    var body: some Scene {
        WindowGroup {
            
            if (!isSplashScreenPresented){
                GalleryView()
                    .environmentObject(galleryVM)
                
                
            }else{
                SplashScreenView(isSplashScreenPresented: $isSplashScreenPresented)
                    .frame(maxWidth: screenWidth ,maxHeight: screenHeight)
                
            }
            
        }
    }
}





