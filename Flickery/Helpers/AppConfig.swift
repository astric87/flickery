//
//  AppConfig.swift
//  Flickery
//
//  Created by Aaron Strickland on 23/11/2023.
//

import Foundation

import Foundation

class AppConfig {
    static let shared = AppConfig()

    private init() {}
    
    
    
    var baseURL: String = "https://api.flickr.com/services/"
    

    func flickrAPIKey() -> String {
        guard let apiKey = Bundle.main.infoDictionary?["FLICKR_API_KEY"] as? String else {
            fatalError("Flickr API key not found. Please add it to the Info.plist file.")
        }
        return apiKey
    }
}
