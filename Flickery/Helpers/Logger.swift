//
//  Logger.swift
//  Flickery
//
//  Created by Aaron Strickland on 23/11/2023.
//

import Foundation


import Foundation

enum LogLevel: String {
    case verbose = "[VERBOSE]"
    case debug = "[DEBUG]"
    case info = "[INFO]"
    case warning = "[WARNING]"
    case error = "[ERROR]"
}

class Logger {
    static var logLevel: LogLevel = .debug

    static func log(_ level: LogLevel, _ message: String, file: String = #file, function: String = #function, line: Int = #line) {
        #if DEBUG
        if level.rawValue == "VERBOSE" || level.rawValue == "DEBUG" {
            print("\(level.rawValue) \(Date()): \(message) (\(file.components(separatedBy: "/").last ?? ""), \(function), line \(line))")
        } else {
            print("\(level.rawValue) \(Date()): \(message)")
        }
        #endif
    }

    static func verbose(_ message: String, file: String = #file, function: String = #function, line: Int = #line) {
        log(.verbose, message, file: file, function: function, line: line)
    }

    static func debug(_ message: String, file: String = #file, function: String = #function, line: Int = #line) {
        log(.debug, message, file: file, function: function, line: line)
    }

    static func info(_ message: String) {
        log(.info, message)
    }

    static func warning(_ message: String) {
        log(.warning, message)
    }

    static func error(_ message: String) {
        log(.error, message)
    }
}
