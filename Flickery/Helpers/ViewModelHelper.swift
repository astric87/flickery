//
//  ViewModelHelper.swift
//  Flickery
//
//  Created by Aaron Strickland on 23/11/2023.
//

import Foundation

class ViewModelHelper {
    static func errorMessage(for error: Error) -> String {
        if let flickrError = error as? FlickrError {
            switch flickrError {
            case .urlConstructionError:
                return "Error constructing URL"
            case .networkError(let description):
                return "Network error: \(description)"
            case .decodingError(let description):
                return "Error decoding data: \(description)"
            }
        } else {
            // Handle other types of errors if necessary
            return "Unknown Error"
        }
    }
}
