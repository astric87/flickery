//
//  Media.swift
//  Flickery
//
//  Created by Aaron Strickland on 21/11/2023.
//

import Foundation





struct Media: Codable {
    let urlString: String?

    // CodingKeys to customise the property names during encoding and decoding.
    enum CodingKeys: String, CodingKey {
        case urlString = "m"
    }

    init(from container: KeyedDecodingContainer<CodingKeys>) throws {
        // Handle the conversion of an empty string to nil for the media URL.
        let urlString = try container.decode(String.self, forKey: .urlString)
        self.urlString = urlString.isEmpty ? nil : urlString
    }
}
