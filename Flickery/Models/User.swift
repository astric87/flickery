//
//  User.swift
//  Flickery
//
//  Created by Aaron Strickland on 22/11/2023.
//

import Foundation

struct UserInfo: Codable {
    let person: FlickrPerson

    struct FlickrPerson: Codable {
        let id: String
        let nsid: String
        let iconserver: String
        let iconfarm: Int
        let username: Username
        let description: Description
        let profileurl: Profileurl?
        let photos: Photos

        struct Username: Codable {
            let _content: String
        }

        struct Description: Codable {
            let _content: String
        }

        struct Profileurl: Codable {
            let _content: String
        }

        struct Photos: Codable {
            let firstdatetaken: Firstdatetaken
            let firstdate: Firstdate
            let count: Count

            struct Firstdatetaken: Codable {
                let _content: String
            }

            struct Firstdate: Codable {
                let _content: String
            }

            struct Count: Codable {
                let _content: Int
            }

            // CodingKeys for nested structures
            private enum CodingKeys: String, CodingKey {
                case firstdatetaken
                case firstdate
                case count
            }

            init(from decoder: Decoder) throws {
                let container = try decoder.container(keyedBy: CodingKeys.self)

                // Decode non-optional properties
                firstdatetaken = try container.decode(Firstdatetaken.self, forKey: .firstdatetaken)
                firstdate = try container.decode(Firstdate.self, forKey: .firstdate)
                count = try container.decode(Count.self, forKey: .count)
            }
        }

        // CodingKeys for FlickrPerson
        private enum CodingKeys: String, CodingKey {
            case id
            case nsid
            case iconserver
            case iconfarm
            case username
            case description
            case profileurl
            case photos
        }

        // Custom decoding strategy to handle empty strings and convert them to nil.
        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)

            // Decode non-optional properties
            id = try container.decode(String.self, forKey: .id)
            nsid = try container.decode(String.self, forKey: .nsid)
            iconserver = try container.decode(String.self, forKey: .iconserver)
            iconfarm = try container.decode(Int.self, forKey: .iconfarm)
            username = try container.decode(Username.self, forKey: .username)
            description = try container.decode(Description.self, forKey: .description)
            profileurl = try container.decodeIfPresent(Profileurl.self, forKey: .profileurl)
            photos = try container.decode(Photos.self, forKey: .photos)
        }
    }
}
