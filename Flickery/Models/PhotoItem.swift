//
//  ImageItem.swift
//  Flickery
//
//  Created by Aaron Strickland on 21/11/2023.
//

import SwiftUI

struct PhotoItem: Codable, Identifiable {
    var id = UUID()
    let title: String
    let link: String
    let dateTaken: String
    let description: String
    let published: String
    let author: String
    let authorId: String
    let tags: String
    let media: Media
    

    // CodingKeys to customise the property names during encoding and decoding.
    private enum CodingKeys: String, CodingKey {
        case title
        case link
        case media
        case dateTaken = "date_taken"
        case description
        case published
        case author
        case authorId = "author_id"
        case tags
    }

    // decoding strategy to handle empty strings
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        // Decode non-optional properties
        title = try container.decode(String.self, forKey: .title)
        link = try container.decode(String.self, forKey: .link)
        dateTaken = try container.decode(String.self, forKey: .dateTaken)
        description = try container.decode(String.self, forKey: .description)
        published = try container.decode(String.self, forKey: .published)
        author = try container.decode(String.self, forKey: .author)
        authorId = try container.decode(String.self, forKey: .authorId)
        tags = try container.decode(String.self, forKey: .tags)

        // Decode media with a custom strategy for handling null values
        let mediaContainer = try container.nestedContainer(keyedBy: Media.CodingKeys.self, forKey: .media)
         media = try Media(from: mediaContainer)
    }
}

// Represents the media object representing the image URL.
