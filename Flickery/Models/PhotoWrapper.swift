//
//  ImageWrapper.swift
//  Flickery
//
//  Created by Aaron Strickland on 21/11/2023.
//

import SwiftUI


struct PhotoWrapper: Codable {
    let title: String
    let link: String
    let description: String
    let modified: String
    let generator: String
    let items: [PhotoItem]

    // CodingKeys to customise the property names during encoding and decoding.
    private enum CodingKeys: String, CodingKey {
        case title
        case link
        case description
        case modified
        case generator
        case items
    }

    // decoding strategy to handle empty strings 
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        // Decode non-optional properties
        title = try container.decode(String.self, forKey: .title)
        link = try container.decode(String.self, forKey: .link)
        description = try container.decode(String.self, forKey: .description)
        modified = try container.decode(String.self, forKey: .modified)
        generator = try container.decode(String.self, forKey: .generator)
        items = try container.decode([PhotoItem].self, forKey: .items)
    }
}
