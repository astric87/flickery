# Flickery


### Overview

Flickry is a basic demonstration of retreiving data from the Flickr API.

It has the following functionality/views

- search using single or multiple tags
- search using phrases
- gallery view
- image detail view
- user profile view
- settings view
- ability to toggle the various search modes (safe, moderate, restricted)


### Architecture

I opted to use MVVM architecture, primarily, as this is the architecture I prefer to work with.  MVVM is well documented with many examples, meaning any roadblocks faced, are usually easily overcome.


 
