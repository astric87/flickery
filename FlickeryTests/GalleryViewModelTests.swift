//
//  ViewModelTest.swift
//  FlickeryTests
//
//  Created by Aaron Strickland on 23/11/2023.
//

import XCTest

@testable import Flickery // Update with your actual app module name

class GalleryViewModelTests: XCTestCase {
    var galleryViewModel: GalleryViewModel!
    var profileViewModel: ProfileViewModel!
    


    override func setUp() {
        super.setUp()
        // Create an instance of GalleryViewModel for testing.
        galleryViewModel = GalleryViewModel()
        profileViewModel = ProfileViewModel()
    }

    // This method is called after each test method is run.
    override func tearDown() {
        // Set the galleryViewModel instance to nil to release resources.
        galleryViewModel = nil
        profileViewModel = nil
        super.tearDown()
    }

    // Test method to check if the getImages method in GalleryViewModel behaves as expected.
    func testGetImagesSuccess() {
        // Given
        // Create an expectation with a description to signal the completion of an asynchronous operation.
        let expectation = XCTestExpectation(description: "Images fetched successfully")

        // When
        // Invoke the getImages method on galleryViewModel, passing a specific parameter for testing.
        galleryViewModel.getImages("test") // You can provide a specific text for testing

        // Then
        // Use DispatchQueue to introduce a delay for handling asynchronous code.
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { // Adjust the delay based on your async operation
            // Assert the expected outcomes after the asynchronous operation is complete.

            // Check if the images array in galleryViewModel has a count of 20.
            XCTAssertEqual(self.galleryViewModel.images.count, 20)

            // Check if the errorMessage in galleryViewModel is nil.
            XCTAssertNil(self.galleryViewModel.errorMessage)

            // Fulfill the expectation to signal the completion of the asynchronous operation.
            expectation.fulfill()
        }

        // Wait for the expectations to be fulfilled within the specified timeout.
        wait(for: [expectation], timeout: 5.0) // Set a timeout based on your expected async operation time
    }
    
}


